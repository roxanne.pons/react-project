import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

// both lines same as:
// ReactDOM.render(<App />, document.getElementById("root"));
const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);

// pour que webpack et react puissent communiquer en continu websocket
module.hot.accept();
