import React from 'react';
import PropTypes from 'prop-types';

const Post = props => {
  const {title, children} = props;
  return (
    <div className="post">
      <h3 className="post__title">{title}</h3>
      <p>{children}</p>
    </div>
  );
};

Post.defaultProps = {
  title: 'Aucun titre défini',
  children: 'Pas de contenu défini',
};
Post.propTypes = {
  title: PropTypes.string,
  children: PropTypes.string.isRequired,
};

export default Post;
