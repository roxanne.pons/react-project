import React from 'react';
import List from './List';

const postsList = [
  {
    title: "Qu'est ce que le DevOps ?",
    content: "C'est plus une sorte de guide",
  },
  {
    title: '10 trucs pour apprendre à mieux coder',
    content: 'La 6e va vous étonner',
  },
  {
    title: 'Le Javascript, réalité ou fiction ?',
    content: 'Quae eius dolor expedita aperiam. Quaerat quis modi cupiditate.',
  },
];

const App = () => {
  return (
    <div className="content">
      <h1>Mon Blog</h1>
      <List posts={postsList} />
    </div>
  );
};

export default App;


