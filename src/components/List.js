import React from 'react';
import PropTypes from 'prop-types';
import Post from './Post';

const List = ({posts}) => {
  return (
    <div className="list">
      {posts.map(post => (
        <Post title={post.title}>{post.content}</Post>
      ))}
    </div>
  );
};

List.defaultProps = {
  posts: 'Il ny a pas de posts.',
};
Post.propTypes = {
  posts: PropTypes.array,
};

export default List;
